import hyperfetch from '../'

describe('hyperfetch', function () {
  it('should fetch real data', async () => {
    const uri = "https://angelo.veltens.org/profile/card#me";
    const result = await hyperfetch(uri);
    const person = result.find(it => it["@id"] === uri);
    expect(person).toEqual({
      "@id": "https://angelo.veltens.org/profile/card#me",
      "@type": "http://schema.org/Person"
    });
  });

});
