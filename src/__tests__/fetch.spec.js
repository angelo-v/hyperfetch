import hyperfetch from '../fetch';

const Readable = require('stream').Readable;

const n3 = `
      PREFIX s: <http://schema.org/>
      <https://person.example> s:name "Jane Doe" .
    `;

const jsonld = `{
  "@context": {
    "s": "http://schema.org/"
  },
  "@id": "https://person.example",
  "s:name": "Jane Doe"
}`;

const rdfxml= `<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF
   xmlns:s="http://schema.org/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
>
  <rdf:Description rdf:about="https://person.example">
    <s:name>Jane Doe</s:name>
  </rdf:Description>
</rdf:RDF>`;

describe.each([
    ['text/turtle', n3],
    ['text/n3', n3],
    ['application/n-quads', n3],
    ['application/n-triples', n3],
    ['application/trig', n3],
    ['application/ld+json', jsonld],
    ['application/rdf+xml', rdfxml],
])('Given a web resource https://person.example serves %s', (contentType, data) => {

  const input = new Readable({
    read: () => {
      input.push(data);
      input.push(null)
    }
  });

  let fetch;
  beforeEach(() => {
    fetch = jest.fn();
    fetch.mockReturnValue(Promise.resolve({
      headers: {
        get: (name) => name === 'content-type' ? contentType : undefined
      },
      body: input
    }))
  });

  describe('when the resource is fetched', () => {

    let result;
    beforeEach(async () => {
      result = await hyperfetch("http://person.example", {fetch});
    });


    it('then the URI is dereferenced and the response body contains JSON-LD', () => {
      expect(fetch).toHaveBeenCalled();
      expect(fetch.mock.calls[0][0]).toEqual("http://person.example");
      expect(result).toEqual([
        {
          "@id": "https://person.example",
          "http://schema.org/name": "Jane Doe"
        }]
      );

    });

  });

});
