const formats = require('@rdfjs/formats-common');
const fetch = require('@rdfjs/fetch-lite');

import {RdfXmlParser} from "rdfxml-streaming-parser";

const SerializerJsonld = require('@rdfjs/serializer-jsonld');

formats.parsers.set('application/rdf+xml', new RdfXmlParser());

export default async (id, options = {}) => {
  const serializerJsonld = new SerializerJsonld();

  return new Promise(
      (resolve) => fetch(id, {
        formats,
        ...options
      })
      .then(res => res.quadStream())
      .then(quadStream => {
        const output = serializerJsonld.import(quadStream);
        output.on('data', jsonld => {
          resolve(jsonld)
        })
      }));
}
