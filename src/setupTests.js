import fetchMock from 'jest-fetch-mock';

global.fetch = fetchMock;

global.console.warn = (message) => {
  throw message
};

global.console.error = (message) => {
  throw message
};