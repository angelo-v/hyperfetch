# Hyperfetch

Hyperfetch tries to fetch JSON-LD from any URI.

## Quickstart

```javascript
import hyperfetch from 'hyperfetch'

hyperfetch('https://api.example/resource').then(jsonld => {
  console.log(jsonld)
});

```

## Supported Content-Types

A resource representation may be delivered as one of the following Content-Types:

 - application/ld+json
 - application/rdf+xml
 - application/trig
 - application/n-quads
 - application/n-triples
 - text/n3
 - text/turtle

**For any supported Content-Type, hyperfetch will return a JSON-LD object!**

Unsupported Content-Types cause an error.

## Passing custom options

You may pass an options object as second argument, to set for example HTTP headers or use a specific implementation of `fetch`.

```javascript
hyperfetch('https://api.example/resource', {
  // custom options
})
```

For details see [documentation of the underlying library @rdfjs/fetch-lite](https://github.com/rdfjs/fetch-lite#options)

## Storing data to hyperfact

The data retrieved by hyperfetch can directly be merged to a [hyperfact](https://www.npmjs.com/package/hyperfact) store:

```javascript

import hyperfetch from 'hyperfetch';
import createStore from 'hyperfact';

export const facts = createStore();

hyperfetch("https://api.example/resource")
  .then(resource => facts.merge(resource))
```
